## Assignment

A sample test assignment for SMS Group.


I've used Laravel 6 for this assignment. I've implemented all the tasks you specified in the assignment. I've implemented this assignment in LAMP stack.
I did create a virtual host configuration for this assignment with ServeName as 'dev.assignment.test'. For it's frontend I've used bootstrap, jquery, axios, 
bootstrap-datepicker and vuejs library. I'll try create a docket compose file for it but if in a case I'll not able to create it follow the 
following steps to run the application.
 
1.Clone this repository using following git command...

	git clone https://pashinde@bitbucket.org/pashinde/assignment.git
 
2.At first copy the .env.example in project's root directory as .env in same directory. Now change following configuration settings in it as per your
environment...

APP_NAME=Assignment
APP_ENV=local
APP_KEY=base64:yFxEMLF2krlg1bUwrSQgCA7gDrLH+IO3KArgqC1z8pg=
APP_DEBUG=true
APP_URL=http://dev.assignment.test

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=assignment
DB_USERNAME=your_database_user_name
DB_PASSWORD=your_database_password

3.Now, run composer install command to install all project dependancies.
	Also set following permissions for project.
	
	- sudo chown -R www-data:www-data assignment
	- sudo chmod -R 775 assignment
	- sudo chmod -R 777 assignment/storage
	- sudo chmod -R 777 assignment/bootstrap/cache

4.Create as virutal host configuration file for apache web server like this...
		
		ServerAdmin pashinde.7@gmail.in
        ServerName dev.assignment.test
        DocumentRoot /var/www/html/assignment/public

        <Directory /var/www/html/assignment/public>
                Options FollowSymLinks MultiViews
                AllowOverride All
                Require all granted
        </Directory>

Then enable the site and reload the apache web server. Change your host file to map this DNS with the your local ip like...

	127.0.0.1 dev.assignment.test
	
Now you will be able to access the appliction from browser.

##Database Seeder

	To seed data into mariadb/mysql database, at first create a database named 'assignment'.
	
	Then run 'php artisan migrate' command to run the migration from the project root directory. After running the migration, records and migration 
	tables will get created in assignment database.
	
	Then run 'php artisan db:seed' command from terminal to seed the database from data.json file which you've provided. This file has placed in 
	database/data/ directory in project folder.

	
## REST Apis

	The base URL to access the REST apis is http://dev.assignment.test/api/records
	
	1.Get records with pagination
		- Method : GET
		- URL : http://dev.assignment.test/api/records?page=1&records=100
		- specify page number and number of records to fetch per request as shown in above URL
		
	2.Get single record
		- Method : GET
		- URL : http://dev.assignment.test/api/records/{record_id}
		- specify id of the record which want to fetch as shown in above URL
		
	3.Create a new record
		- Method : POST
		- URL : http://dev.assignment.test/api/records
		- Provide fillowing input fields with specified rules to create a new record
			- city : required, alphabetic and maximum 255 characters
			- start_date : required, date
			- end_date : required, date and it must be date after the start date
			- price : required and numeric value
			- status : required, alphabetic and maximum 255 charactes
			- color : required, text and maximum 255 charactes
			
	4.Update a record
		- Method : PUT / PATCH
		- URL : http://dev.assignment.test/api/records/{record_id}
		- Provide fillowing input fields with specified rules to update a record
			- city : required, alphabetic and maximum 255 characters
			- start_date : required, date
			- end_date : required, date and it must be date after the start date
			- price : required and numeric value
			- status : required, alphabetic and maximum 255 charactes
			- color : required, text and maximum 255 charactes
			
	5.Delete a record
		- Method : DELETE
		- URL : http://dev.assignemt.test/api/records/{record_id}
		- Specify the record id which you want to delete
		
##View Records

	- Navigating to home page i.e. to http://dev.assignment.test, you'll see records with pagination.
	- To sort the records by column just click on the column head.
	- Change number of records to show per page.
	- Select a date range to fetch records within selected range with pagination.
	
##FrontEnd

	As my devlopement machine is not updated, so it has old version of nodejs and npm. I've many applications which are using this nodejs and npm. 
	So to continue to work on these applications I could not update these to latest versions. The current laravel-mix conflicts with these version of nodejs, 
	so for frontend I've not used it. I've included all required frontend libraries from their respective CDN's.