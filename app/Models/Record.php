<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $table = 'records';

    protected $fillable = [
        'city',
        'start_date',
        'end_date',
        'price',
        'status',
        'color'
    ];

    public $timestamps = false;
}
