<?php

namespace App\Http\Controllers;

use App\Models\Record;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(Request $request){

        //if($request->ajax()){

            $no_of_records = e($request->get('no_of_records'));
            $start_date_in = e($request->get('start_date'));
            $end_date_in = e($request->get('end_date'));

            $records = new Record();
            if( ! empty($start_date_in)){

                $start_date = Carbon::createFromFormat('d/m/Y', $start_date_in);
                $end_date = Carbon::createFromFormat('d/m/Y', $end_date_in);

                $records = Record::where(function($q) use ($start_date){
                    $q->where('start_date', '>=', date("Y-m-d", strtotime($start_date)));
                })->where(function($q) use ($end_date) {
                    $q->where('end_date', '<=', date("Y-m-d", strtotime($end_date)));
                })->paginate($no_of_records);

            } else {
                $records = Record::paginate($no_of_records);
            }

            if($records->count() <= 0){

                return response()->json(['success' => false, 'data' => []]);

            } else {

                $current_page = $records->currentPage();
                $last_page = $records->lastPage();
                $per_page = $records->perPage();
                $total = $records->total();

                $offset = $per_page * $current_page - $per_page;
                $from = $offset + 1;
                $to = $from + $records->count() - 1;

                $path = $records->resolveCurrentPath();

                $first_page_url = $path . "?page=1&start_date=$start_date_in&end_date=$end_date_in";
                $next_page_url = null;
                $prev_page_url = null;
                $last_page_url = $path . "?page=$last_page&start_date=$start_date_in&end_date=$end_date_in";

                if($records->count() < $per_page){//last request
                    $next_page_url = null;
                    $prev_page_url = ($current_page == 1) ? null : $path . "?page=". ($current_page - 1) . "&start_date=$start_date_in&end_date=$end_date_in";
                }
                else{//has more requests
                    $next_page_url = $path . "?page=" . ($current_page + 1) . "&start_date=$start_date_in&end_date=$end_date_in";
                    $prev_page_url = ($current_page == 1) ? null : $path . "?page=" . ($current_page - 1) . "&start_date=$start_date_in&end_date=$end_date_in";
                }

                return response()->json(['success' => true, 'data' => $records, 'current_page' => $current_page, 'first_page_url' => $first_page_url,
                    'from' => $from, 'to' => $to, 'last_page' => $last_page, 'last_page_url' => $last_page_url,
                    'next_page_url' => $next_page_url, 'path' => $path, 'per_page' => $per_page, 'prev_page_url' => $prev_page_url,
                    'total' => $total]);
            }
//        } else {
//
//            return view('home');
//
//        }
    }
}
