<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use App\Models\Record;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource with pagination.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get records with pagination
        $page_number = e($request->get('page'));
        $records =  e($request->get('records'));
        $skip = 0;

        if(! is_numeric($page_number) && ! is_numeric($records)){
            return response()->json(['success' => false, 'message' => 'Bad Request.']);
        }

        $total_records = Record::count('*');

        if($total_records == 0){
            return response()->json(['success' => false, 'message' => 'No records found.']);
        } else {

                $total_pages = ceil($total_records / $records);

                 if($total_pages >= $page_number){

                     $skip = $records * ($page_number - 1);

                 } else {

                     return response()->json(['success' => false, 'message'=>'No more records']);

                 }

                 $records = Record::skip($skip)->take($records)->get();

                 return response()->json(['success' => true, 'records' => $records->toArray()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $rules = [
            'city' => 'required|alpha|max:255',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'price' => 'required|numeric',
            'status' => 'required|alpha|max:255',
            'color' => 'required|string|max:255'
        ];

        $v = Validator::make($input, $rules);
        if($v->fails()){
            return response()->json(['success' => false, 'validation_errors' => true, 'errors' => $v->getMessageBag()->getMessages()]);
        } else {

            $start_date = Carbon::createFromFormat('m/d/Y', $input['start_date']);

            $end_date = Carbon::createFromFormat('m/d/Y', $input['end_date']);

            $record = Record::create([
                'city' => e($input['city']),
                'start_date' => $start_date,
                'end_date' => $end_date,
                'price' => e($input['price']),
                'status' => e($input['status']),
                'color' => e($input['color'])
            ]);

            $records_arr = [
                'city' => $record->city,
                'start_date' => date_format($record->start_date, 'm/d/Y'),
                'end_date' => date_format($record->end_date, 'm/d/Y'),
                'price' => $record->price,
                'status' => $record->status,
                'color' => $record->color
            ];

            return response()->json(['success' => true, 'record' => $records_arr, 'message' => 'Record created successfully.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = Record::find($id);

        if(! $record){
            return response()->json(['success' => false, 'message' => 'Record not found.']);
        } else {

            return response()->json(['success' => true, 'record' => $record->toArray()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $record = Record::find($id);

        if(! $record){
            return response()->json(['success' => false, 'message' => 'Record not found.']);
        } else {

            $input = $request->all();

            $rules = [
                'city' => 'required|alpha|max:255',
                'start_date' => 'required|date',
                'end_date' => 'required|date|after:start_date',
                'price' => 'required|numeric',
                'status' => 'required|alpha|max:255',
                'color' => 'required|string|max:255'
            ];

            $v = Validator::make($input, $rules);
            if($v->fails()){
                return response()->json(['success' => false, 'validation_errors' => true, 'errors' => $v->getMessageBag()->getMessages()]);
            } else {

                $start_date = Carbon::createFromFormat('m/d/Y', $input['start_date']);

                $end_date = Carbon::createFromFormat('m/d/Y', $input['end_date']);

                $record->city = e($input['city']);
                $record->start_date = $start_date;
                $record->end_date = $end_date;
                $record->price = e($input['price']);
                $record->status = e($input['status']);
                $record->color = e($input['color']);

                $record->save();

                $records_arr = [
                    'city' => $record->city,
                    'start_date' => date_format($record->start_date, 'm/d/Y'),
                    'end_date' => date_format($record->end_date, 'm/d/Y'),
                    'price' => $record->price,
                    'status' => $record->status,
                    'color' => $record->color
                ];

                return response()->json(['success' => true, 'record' => $records_arr,
                'message' => 'Record updated successfully.']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Record::find($id);

        if(! $record){
            return response()->json(['success' => false, 'message' => 'Record not found.']);
        } else {

            $record->delete();

            return response()->json(['success' => true, 'message' => 'Record deleted successfully.']);
        }
    }
}
