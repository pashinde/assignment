<?php

use Illuminate\Database\Seeder;
use App\Models\Record;
use Carbon\Carbon;

class RecordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('records')->delete();

        $json = File::get('database/data/data.json');

        $data = json_decode($json);

        foreach($data as $obj):

            $start_date = Carbon::createFromFormat('m/d/Y', $obj->start_date);
            $end_date = Carbon::createFromFormat('m/d/Y', $obj->end_date);

            Record::create([
                'id' => $obj->id,
                'city' => $obj->city,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'price' => $obj->price,
                'status' => $obj->status,
                'color' => $obj->color
            ]);
        endforeach;
    }
}
