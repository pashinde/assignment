<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Assignment') }}</title>

    <!-- Styles -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <style>
        #main{
            padding-top: 0 !important;
            padding-bottom: 0 !important;
        }
        #content{
            padding-top:1em;
        }
        thead tr{
            background-color: rgba(0, 0, 0, 0.05) !important;
        }
        tbody tr:nth-child(odd){
            background-color: rgba(255,255,255, 1) !important;
        }
        tbody tr:nth-child(even){
            background-color: #f2f2f2 !important;
        }
        form, section{
            padding: .5em;
            background-color: #fff;
        }
        div.form-group{
            margin-bottom: 0;
        }
        #loading-cntnr{
            height: 100vh;
            align-items: center;
            display: flex;
            justify-content: center;
        }
        div.margin-bottom-1, form {
            margin-bottom: 1em;
        }
        input.submit-btn{
            width:auto !important;
            padding-top: 0;
            padding-bottom: 0;
        }
        #heading{
            padding-bottom: 5px;
            border-bottom: 1px solid lightgrey;
        }
        #heading h4{
            margin-bottom: 0;
            padding-top: .5em;
        }
        input{
            width:100%;
            height:2.1em;
        }
        #pagination-btns-cntnr button{
            border-radius: 0;
        }
        #pagination-btns-cntnr button.pg-btn{
            color: #005cbf;
            background-color: unset;
        }
        #pagination-btns-cntnr button.active{
            background-color: #fff;
            border: 1px solid lightgray;
        }
        #tbody tr,
        .tbody tr{
            display: table-row;
        }
        #tbody tr td,
        .tbody tr td{
            padding: 0.30em !important;
            vertical-align: middle;
        }
        #thead tr th{
            padding: 0.30em !important;
        }
        h3.no-records{
            text-align: center;
        }
        thead span{
            width:15px;
            height: 20px;
            float:right;
            z-index: 99999999;
        }
        th.asc span{
            background-color: #fff;
            background-repeat: no-repeat;
            background-image: url('/images/down.jpg') !important;
        }
        th.desc span{
            background-color: #fff;
            background-repeat: no-repeat;
            background-image: url('/images/up.jpg') !important;
        }
    </style>
</head>
<body style="background-color: #ebebe0;">
<div id="app">
    <main id="main" class="py-4">
        <div class="container-fluid">
            <div class="row">
                <div id="content" class="col-md-10 offset-1">
                    <records></records>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>
    function sortTable(event, n) {
        let $currentTarget = $(event.currentTarget);
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("records-table");
        switching = true;
        // Set the sorting direction to ascending:
        dir = "asc";
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /* Check if the two rows should switch place,
                based on the direction, asc or desc: */
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }

                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }

            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                // Each time a switch is done, increase this count by 1:
                switchcount ++;
            } else {
                /* If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again. */
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }

        if($currentTarget.hasClass('sorted')){

            $('#records-table tr th').removeClass('asc').removeClass('desc');

            $currentTarget.addClass(dir)

        } else {

            $('#records-table tr th').removeClass('sorted').removeClass('asc').removeClass('desc');

            $currentTarget.addClass('sorted').addClass(dir)
        }
    }

    Vue.component('records', {
        data: function(){
            return {
                loading: false,
                records: [],
                records_arr: [],
                no_of_records: 50,
                current_page: 1,
                first_page_url: undefined,
                from_records: 0,
                last_page: 0,
                last_page_url: undefined,
                next_page_url: undefined,
                path: undefined,
                per_page: 0,
                prev_page_url: undefined,
                to_records: 0,
                total: 0,
                no_of_pages: 0,
                current_group: 0,
                total_groups: 0,
                pages_arr: [],
                current_active: '1',
                has_records: -1,
                start_date: '',
                end_date: '',
                has_error: false,
                error_message: ''
            }
        },
        methods: {
            getRecordsFirstPage: function(e){
                e.preventDefault();

                let $currentTarget = $(e.currentTarget),
                    url = $currentTarget.attr('url');

                if(url !== undefined && this.current_page != 1){

                    this.loading = true;

                    axios.get(url, {
                        params: {
                            no_of_records: this.no_of_records
                        }
                    }).then((response) => {

                        this.current_group = 0;
                        this.records = response.data.data.data;
                        this.records_arr = response.data.data.data;

                        if(this.records.length){
                            this.has_records = 1;
                        } else {
                            this.has_records = 0;
                        }

                        this.current_active = 1;

                        this.current_page = response.data.current_page;
                        this.first_page_url = response.data.first_page_url;
                        this.from_records = response.data.from;
                        this.last_page = response.data.last_page;
                        this.last_page_url = response.data.last_page_url;
                        this.next_page_url = response.data.next_page_url;
                        this.path = response.data.path;
                        this.per_page = response.data.per_page;
                        this.prev_page_url = response.data.prev_page_url;
                        this.to_records = response.data.to;
                        this.total = response.data.total;

                        this.loading = false;

                    }).catch((error) => {

                        this.loading = false;
                    });
                }
            },
            getRecordsPrevPage: function(e){
                e.preventDefault();

                let $currentTarget = $(e.currentTarget),
                    url = $currentTarget.attr('url');

                if (url !== undefined && this.current_page != 1) {

                    this.loading = true;

                    axios.get(url, {
                        params: {
                            no_of_records: this.no_of_records
                        }
                    }).then((response) => {

                        let temp_curent_page = this.current_page - 1;

                        if(temp_curent_page % 10 == 0){
                            this.current_group--;
                        }

                        this.current_active--;

                        this.records = response.data.data.data;
                        this.records_arr = response.data.data.data;

                        if(this.records.length){
                            this.has_records = 1;
                        } else {
                            this.has_records = 0;
                        }

                        this.current_page = response.data.current_page;
                        this.first_page_url = response.data.first_page_url;
                        this.from_records = response.data.from;
                        this.last_page = response.data.last_page;
                        this.last_page_url = response.data.last_page_url;
                        this.next_page_url = response.data.next_page_url;
                        this.path = response.data.path;
                        this.per_page = response.data.per_page;
                        this.prev_page_url = response.data.prev_page_url;
                        this.to_records = response.data.to;
                        this.total = response.data.total;

                        this.loading = false;

                    }).catch((error) => {

                        this.loading = false;
                    });
                }
            },
            getRecordsNextPage: function(e){
                e.preventDefault();

                let $currentTarget = $(e.currentTarget),
                    url = $currentTarget.attr('url');

                if (url !== undefined && this.current_page != this.last_page) {

                    this.loading = true;

                    axios.get(url, {
                        params: {
                            no_of_records: this.no_of_records
                        }
                    }).then((response) => {

                        if(this.current_page % 10 == 0){
                            this.current_group++;
                        }

                        this.current_active++;

                        this.records = response.data.data.data;
                        this.records_arr = response.data.data.data;

                        if(this.records.length){
                            this.has_records = 1;
                        } else {
                            this.has_records = 0;
                        }

                        this.current_page = response.data.current_page;
                        this.first_page_url = response.data.first_page_url;
                        this.from_records = response.data.from;
                        this.last_page = response.data.last_page;
                        this.last_page_url = response.data.last_page_url;
                        this.next_page_url = response.data.next_page_url;
                        this.path = response.data.path;
                        this.per_page = response.data.per_page;
                        this.prev_page_url = response.data.prev_page_url;
                        this.to_records = response.data.to;
                        this.total = response.data.total;

                        this.loading = false;

                    }).catch((error) => {

                        this.loading = false;
                    });

                }
            },
            getRecordsLastPage: function(e){
                e.preventDefault();

                let $currentTarget = $(e.currentTarget),
                    url = $currentTarget.attr('url');

                if (url !== undefined && this.current_page != this.last_page) {

                    this.loading = true;

                    axios.get(url, {
                        params: {
                            no_of_records: this.no_of_records
                        }
                    }).then((response) => {

                        let arr_index = this.pages_arr[this.total_groups - 1].length;
                        this.current_active = (this.pages_arr[this.total_groups - 1][arr_index - 1]);

                        this.current_group = (this.pages_arr.length - 1);

                        this.records = response.data.data.data;
                        this.records_arr = response.data.data.data;

                        if(this.records.length){
                            this.has_records = 1;
                        } else {
                            this.has_records = 0;
                        }

                        this.current_page = response.data.current_page;
                        this.first_page_url = response.data.first_page_url;
                        this.from_records = response.data.from;
                        this.last_page = response.data.last_page;
                        this.last_page_url = response.data.last_page_url;
                        this.next_page_url = response.data.next_page_url;
                        this.path = response.data.path;
                        this.per_page = response.data.per_page;
                        this.prev_page_url = response.data.prev_page_url;
                        this.to_records = response.data.to;
                        this.total = response.data.total;

                        this.loading = false;

                    }).catch((error) => {

                        this.loading = false;
                    });
                }
            },
            getRecordsPage: function(e){
                e.preventDefault();

                let $currentTarget = $(e.currentTarget),
                    url = $currentTarget.attr('url'),
                    current_no = $currentTarget.attr('data-number');

                let start_date = $('input[name="start_date"]').val(),
                    end_date = $('input[name="end_date"]').val();

                if(url !== undefined && this.current_page != current_no){

                    this.loading = true;

                    $('button.pg-no-btn').removeClass('active');
                    this.current_active = current_no

                    axios.get(url, {
                        params: {
                            no_of_records: this.no_of_records,
                            start_date: start_date,
                            end_date: end_date
                        }
                    }).then((response) => {

                        this.records = response.data.data.data;
                        this.records_arr = response.data.data.data;

                        if(this.records.length){
                            this.has_records = 1;
                        } else {
                            this.has_records = 0;
                        }

                        this.current_page = response.data.current_page;
                        this.first_page_url = response.data.first_page_url;
                        this.from_records = response.data.from;
                        this.last_page = response.data.last_page;
                        this.last_page_url = response.data.last_page_url;
                        this.next_page_url = response.data.next_page_url;
                        this.path = response.data.path;
                        this.per_page = response.data.per_page;
                        this.prev_page_url = response.data.prev_page_url;
                        this.to_records = response.data.to;
                        this.total = response.data.total;

                        this.loading = false;

                    }).catch((error) => {

                        this.loading = false;
                    });
                }
            },
            noOfRecordsChanged: function(e){
                this.loading = true;

                let $currentTarget = $(e.currentTarget);
                this.no_of_records = $currentTarget.val();

                let start_date = $('input[name="start_date"]').val(),
                    end_date = $('input[name="end_date"]').val();

                axios.get('/records', {
                    params: {
                        no_of_records: this.no_of_records,
                        start_date: start_date,
                        end_date: end_date
                    }
                }).then((response) => {

                    this.records = response.data.data.data;
                    this.records_arr = response.data.data.data;

                    if(this.records.length){
                        this.has_records = 1;
                    } else {
                        this.has_records = 0;
                    }

                    this.current_page = response.data.current_page;
                    this.first_page_url = response.data.first_page_url;
                    this.from_records = response.data.from;
                    this.last_page = response.data.last_page;
                    this.last_page_url = response.data.last_page_url;
                    this.next_page_url = response.data.next_page_url;
                    this.path = response.data.path;
                    this.per_page = response.data.per_page;
                    this.prev_page_url = response.data.prev_page_url;
                    this.to_records = response.data.to;
                    this.total = response.data.total;

                    this.no_of_pages = Math.ceil(this.total / this.per_page);

                    let total_pages = this.no_of_pages,
                        counter = 1,
                        j = 0,
                        i = 0;

                    this.current_group = 0;
                    this.total_groups = 0;
                    this.pages_arr = [];
                    this.current_active = '1';

                    if(total_pages > 0){
                        for(i = 0; i < total_pages; i++){

                            if(i % 10 == 0){
                                this.pages_arr[this.total_groups] = [];
                                this.total_groups++;
                                j = 0;
                            }

                            this.pages_arr[this.total_groups - 1][j] = counter;
                            j++;
                            counter++;
                        }
                    }

                    this.loading = false;

                }).catch((error) => {

                    this.loading = false;
                });
            },
            getRecords: function(e){

                let start_date = $('input[name="start_date"]').val(),
                    end_date = $('input[name="end_date"]').val();

                if(start_date != '' && end_date == ''){
                    this.has_error = true;
                    this.error_message = 'Select End Date';
                    return false;
                }

                this.has_error = false;
                this.error_message = '';

                axios.get('/records', {
                    params: {
                        no_of_records: this.no_of_records,
                        start_date: start_date,
                        end_date: end_date
                    }
                }).then((response) => {

                    this.records = response.data.data.data;
                    this.records_arr = response.data.data.data;

                    if(this.records instanceof Array && this.records.length){
                        this.has_records = 1;

                        this.current_page = response.data.current_page;
                        this.first_page_url = response.data.first_page_url;
                        this.from_records = response.data.from;
                        this.last_page = response.data.last_page;
                        this.last_page_url = response.data.last_page_url;
                        this.next_page_url = response.data.next_page_url;
                        this.path = response.data.path;
                        this.per_page = response.data.per_page;
                        this.prev_page_url = response.data.prev_page_url;
                        this.to_records = response.data.to;
                        this.total = response.data.total;

                        this.no_of_pages = Math.ceil(this.total / this.per_page);

                        let total_pages = this.no_of_pages,
                            counter = 1,
                            j = 0,
                            i = 0;

                        this.current_group = 0;
                        this.total_groups = 0;
                        this.pages_arr = [];
                        this.current_active = '1';

                        if(total_pages > 0){
                            for(i = 0; i < total_pages; i++){

                                if(i % 10 == 0){
                                    this.pages_arr[this.total_groups] = [];
                                    this.total_groups++;
                                    j = 0;
                                }

                                this.pages_arr[this.total_groups - 1][j] = counter;
                                j++;
                                counter++;
                            }
                        }

                    } else {
                        this.has_records = 0;
                    }

                    this.loading = false;

                }).catch((error) => {

                    this.loading = false;
                });
            }
        },
        mounted: function(){
            $('input[name="start_date"]').datepicker({
                format: 'dd/mm/yyyy'
            }).on('changeDate', dateChanged);

            function dateChanged(ev) {
                $(this).datepicker('hide');
                this.start_date = $(this).val();

                $('input[name="end_date"]').datepicker({
                    format: 'dd/mm/yyyy',
                    startDate: $(this).val()
                }).on('changeDate', endDateChanged);
            }

            function endDateChanged(e){
                $(this).datepicker('hide');

                this.has_error = false;
                this.error_message = '';

                this.end_date = $(this).val();
            }
        },
        created: function(){
            axios.get('/records', {
                params: {
                    no_of_records: this.no_of_records,
                    start_date: '',
                    end_date: ''
                }
            }).then((response) => {

                if(response.data.success){

                    $('input.remove').remove();

                    this.records = response.data.data.data;
                    this.records_arr = response.data.data.data;

                    if(this.records.length){
                        this.has_records = 1;

                        this.current_page = response.data.current_page;
                        this.first_page_url = response.data.first_page_url;
                        this.from_records = response.data.from;
                        this.last_page = response.data.last_page;
                        this.last_page_url = response.data.last_page_url;
                        this.next_page_url = response.data.next_page_url;
                        this.path = response.data.path;
                        this.per_page = response.data.per_page;
                        this.prev_page_url = response.data.prev_page_url;
                        this.to_records = response.data.to;
                        this.total = response.data.total;

                        this.no_of_pages = Math.ceil(this.total / this.per_page);

                        let total_pages = this.no_of_pages,
                            counter = 1,
                            j = 0,
                            i = 0;

                        this.current_group = 0;
                        this.total_groups = 0;
                        this.pages_arr = [];
                        this.current_active = '1';

                        if(total_pages > 0){
                            for(i = 0; i < total_pages; i++){

                                if(i % 10 == 0){
                                    this.pages_arr[this.total_groups] = [];
                                    this.total_groups++;
                                    j = 0;
                                }

                                this.pages_arr[this.total_groups - 1][j] = counter;
                                j++;
                                counter++;
                            }
                        }

                    } else {
                        this.has_records = 0;
                    }

                } else {
                    this.has_records = 0;
                }

                this.loading = false;

            });

        },
        template: `<div class="row">
        <div id="heading" class="col-md-11">
            <div class="row">
                <div class="col-md-12">
                    <h4>Records</h4>
                </div>
            </div>
        </div>
        <div id="content-wrapper" class="col-md-11">
            <div v-if="this.has_error" class="row">
                <div class="col-md-12">
                    <p style="color:red">@{{ this.error_message }}</p>
                </div>
            </div>
            <form v-on:submit.prevent="getRecords" accept-charset="UTF-8">
                <div class="row">
                    <div class="col-md-3">
                           <input type="text" name="start_date" autocomplete="off" placeholder="Select start date">
                    </div>
                    <div class="form-group col-md-3">
                        <input type="text" name="end_date" autocomplete="off" placeholder="select end date">
                    </div>
                    <div class="col-md-3">
                        <input type="submit" value="Search" class="btn btn-info submit-btn"/>
                    </div>
                </div>
            </form>
            <div class="row margin-bottom-1">
                <div class="col-md-4 offset-8">
                    <label>No. of Records : </label>
                    <select v-model="no_of_records" v-on:change="noOfRecordsChanged" class="form-control width-50">
                        <option value="10">10</option>
                        <option value="50">50</option>
                        <option v-if="total >= 100" value="100">100</option>
                        <option v-if="total >= 250" value="250">250</option>
                    </select>
                </div>
            </div>
            <div v-if="loading" class="row">
                <div id="loading-cntnr" class="col-md-12">
                    <img src="/images/loading.gif" />
                </div>
            </div>
            <div v-else class="row">
                <div v-if="has_records !== -1 && has_records === 1" class="col-md-12">
                    <table id="records-table" class="table">
                        <thead  id="thead">
                        <tr>
                            <th v-on:click="sortTable(event, 0)">City<span></span></th>
                            <th v-on:click="sortTable(event, 1)">Start Date<span></span></th>
                            <th v-on:click="sortTable(event, 2)">End Date<span></span></th>
                            <th v-on:click="sortTable(event, 3)">Price<span></span></th>
                            <th v-on:click="sortTable(event, 4)">Status<span></span></th>
                            <th v-on:click="sortTable(event, 5)">Color<span></span></th>
                        </tr>
                        </thead>
                        <tbody id="tbody">
                        <tr v-for="record in records_arr">
                            <td>@{{ record.city }}</td>
                            <td>@{{ record.start_date }}</td>
                            <td>@{{ record.end_date }}</td>
                            <td>@{{ record.price }}</td>
                            <td>@{{ record.status }}</td>
                            <td>@{{ record.color }}</td>
                        </tr>
                        </tbody>
                    </table>
                    <div v-if="has_records" class="row">
                        <div id="pagination-btns-cntnr" class="col-md-8">
                            <button id="first" v-bind:url="first_page_url" v-on:click="getRecordsFirstPage" v-bind:class="(current_page == 1) ? 'btn pg-btn disabled' : 'btn pg-btn'"> << </button>
                            <button id="prev" v-bind:url="prev_page_url" v-bind:data-number="(current_page == 1) ? 'null' : current_page" v-bind:class="(current_page == 1) ? 'btn pg-btn disabled' : 'btn pg-btn'" v-on:click="getRecordsPrevPage"> < </button>
                            <button v-for="(n, index) in this.pages_arr[this.current_group]" v-bind:data-number="n" v-bind:url="path + '?page=' + n"
                                    v-bind:class="(n == current_active) ? 'btn pg-btn pg-no-btn active' : 'btn pg-btn pg-no-btn'" v-on:click="getRecordsPage">@{{ n }}</button>
                            <button id="next" v-bind:url="next_page_url" v-bind:data-number="(current_page == last_page) ? 'null' : current_page" v-bind:class="(current_page == last_page) ? 'btn pg-btn disabled' : 'btn pg-btn'" v-on:click="getRecordsNextPage"> > </button>
                            <button id="last" v-bind:url="last_page_url" v-bind:class="(current_page == last_page) ? 'btn pg-btn disabled' : 'btn pg-btn'" v-on:click="getRecordsLastPage"> >> </button>
                        </div>
                        <div class="col-md-4">
                            <p><span>Showing @{{ this.from_records }} to @{{ this.to_records }} of @{{ this.total }} records</span></p>
                        </div>
                    </div>
                </div>
                <div v-if="has_records !== -1 && has_records === 0" class="col-md-12">
                    <h3 class="no-records">No records found.</h3>
                </div>
            </div>
        </div>
    </div>`
    });

    var app = new Vue({
        el: '#app'
    });
</script>
</body>
</html>
